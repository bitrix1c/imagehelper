<?

/*
 * *
 *  * Created by PhpStorm.
 *  * User: Anton Egorov
 *  * Copyright (c)  2021
 *
 */

namespace AntonYe\Helper;

class ImageHelper {

    /**
     * @var ImageHelper
     */
    protected static $instance = null;

    /**
     * @var bool
     */
    private $isPng = true;

    public static function getInstance() : ImageHelper{
        if (self::$instance == null) {
            self::$instance = new ImageHelper();
        }

        return self::$instance;
    }


    /**
     * - Метод принимает массив файла изображения, которое нужно преобразовать в WEBP
     * - Метод сохраняет изображение по пути ./upload/webp в зависимости от исходного пути файла
     * - Метод возвращает массив файла Битрикс для изображения WEBP
     *
     * @param $array - Массив файла изображения в формате PNG и JPEG, который можно получить с помощью метода CFile::GetFileArray
     * @param int $intQuality - качество сжатия, по-умолчанию - 70%
     * @return array - массив файл
     */
    public function getWebp($array, $intQuality = 70)
    {
        if ($this->checkFormat($array['CONTENT_TYPE']))
        {
            $array['WEBP_PATH'] = $this->generateSrc($array['SRC']);

            if ($this->isPng)
            {
                $array['WEBP_FILE_NAME'] = str_replace('.png', '.webp', strtolower($array['FILE_NAME']));
            }
            else
            {
                $array['WEBP_FILE_NAME'] = str_replace('.jpg', '.webp', strtolower($array['FILE_NAME']));
                $array['WEBP_FILE_NAME'] = str_replace('.jpeg', '.webp', strtolower($array['WEBP_FILE_NAME']));
            }

            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_PATH']))
            {
                if (!mkdir($concurrentDirectory = $_SERVER['DOCUMENT_ROOT'] . $array['WEBP_PATH'], 0777, true) && !is_dir($concurrentDirectory)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                }
            }

            $array['WEBP_SRC'] = $array['WEBP_PATH'] . $array['WEBP_FILE_NAME'];

            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC']))
            {
                if ($this->isPng)
                {
                    $image = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . $array['SRC']);
                }
                else
                {
                    $image = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . $array['SRC']);
                }
                imagewebp($image, $_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC'], $intQuality);
                imagedestroy($image);
                if (filesize($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC']) % 2 == 1)
                {
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC'], "\0", FILE_APPEND);
                }
            }
        }
        return $array;
    }

    /**
     * @param $file - полный путь к файлу или массив файла битрикс, который можно получить с помощью метода CFile::GetFileArray
     * @param $width
     * @param $height
     * @param bool $isProportional
     * @param int $intQuality
     * @return mixed
     */
    public function getResizeWebp($file, $width, $height, $isProportional = true, $intQuality = 70)
    {
        $file['SRC'] = $this->resizePict($file, $width, $height, $isProportional, $intQuality);
        $file = $this->getWebp($file, $intQuality);
        return $file;
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @param bool $isProportional
     * @param int $intQuality
     * @return mixed
     */
    public function getResizeWebpSrc($file, $width, $height, $isProportional = true, $intQuality = 70)
    {
        $file = $this->getResizeWebp($file, $width, $height, $isProportional, $intQuality);
        return $file['WEBP_SRC'];
    }

    /**
     * @param $str
     * @return bool
     */
    private function checkFormat($str)
    {
        if ($str === 'image/png')
        {
            $this->isPng = true;
            return true;
        } elseif ($str === 'image/jpeg')
        {
            $this->isPng = false;
            return true;
        }
        else return false;
    }

    /**
     * @param $arr
     * @return string
     */
    private function implodeSrc($arr)
    {
        $arr[count($arr) - 1] = '';
        return implode('/', $arr);
    }

    /**
     * @param $str
     * @return array|false|string|string[]
     */
    private function generateSrc($str)
    {
        $arPath = explode('/', $str);

        if ($arPath[2] === 'resize_cache')
        {
            $arPath = $this->implodeSrc($arPath);
            return str_replace('resize_cache/iblock', 'webp/resize_cache', $arPath);
        }
        else
        {
            $arPath = $this->implodeSrc($arPath);
            return str_replace('upload/iblock', 'upload/webp/iblock', $arPath);
        }
    }

    /**
     * @param $file - полный путь к файлу или массив файла битрикс, который можно получить с помощью метода CFile::GetFileArray
     * @param $width
     * @param $height
     * @param bool $isProportional
     * @param int $intQuality
     * @return mixed
     */
    private function resizePict($file, $width, $height, $isProportional = true, $intQuality = 70)
    {
        $file = \CFile::ResizeImageGet($file, array('width'=>$width, 'height'=>$height), ($isProportional ? BX_RESIZE_IMAGE_PROPORTIONAL : BX_RESIZE_IMAGE_EXACT), false, false, false, $intQuality);
        return $file['src'];
    }

}